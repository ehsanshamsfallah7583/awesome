import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
} from 'react-native';
import {Actions} from 'react-native-router-flux';

const Login = () => {
  return (
    <View>
      <View style={styles.container}>
        <TextInput placeholder="User Name" />
      </View>

      <TouchableOpacity onPress={() => Actions.push('profile')}>
        <Text>Submit</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});
