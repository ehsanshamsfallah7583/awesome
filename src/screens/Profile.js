import React from 'react';
import {View, Text} from 'react-native';
import {Actions} from 'react-native-router-flux';

const Profile = () => {
  return (
    <View>
      <Text onPress={() => Actions.push('profile')}>Profile</Text>
    </View>
  );
};

export default Profile;
