import React from 'react';
import {Router, Scene, Stack} from 'react-native-router-flux';

import Profile from '../screens/Profile';
import Login from '../screens/Login';

const RouterApp = () => {
  return (
    <Router>
      <Stack key="root">
        <Scene key="login" component={Login} title="Login" />
        <Scene back key="profile" component={Profile} title="Profile" />
      </Stack>
    </Router>
  );
};

export default RouterApp;
